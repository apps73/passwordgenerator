package com.app.passwordgenerator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private TextView tv_ShowPW;
    private Button bt_GeneratePW;
    private String letters[];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        letters = new String[]{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
        tv_ShowPW = findViewById(R.id.tv_ShowPW);
        bt_GeneratePW = findViewById(R.id.bt_GeneratePW);
        bt_GeneratePW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random random = new Random();
                int pw = random.nextInt(8999)+1000;
                String letter = letters[random.nextInt(letters.length-1)];
                tv_ShowPW.setText(letter + Integer.toString(pw));
            }
        });
    }
}
